package kzhou.cuc.service.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kzhou.cuc.ss.service.IService;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.Gson;

/**
 * Servlet implementation class ServiceServlet
 */
public class ServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private WebApplicationContext ctx = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServiceServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		ctx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String method = request.getParameter("method");
		IService service = (IService)ctx.getBean("pms4aService");
		if(method.equals("login")){
			//登陆
			this.login(service,request,response);
		}else if(method.equals("loginByForm")){
			//表单登录
			this.loginByForm(service,request,response);
		}else if(method.equals("myPendingTask")){
			//待办任务
			this.myPendingTask(service, request, response);
		}else if(method.equals("getTaskInfoById")){
			//根据任务id查询任务详细信息
			this.getTaskDetailById(service, request, response);
		}else if(method.equals("saveTask")){
			//任务保存
			this.saveTask(service, request, response);
		}else if(method.equals("submitTask")){
			//任务提交
			this.submitTask(service, request, response);
		}else if(method.equals("myParticipateProjList")){
			//归档任务-我参与的项目
			this.myParticipateProjList(service, request, response);
		}else if(method.equals("myCompleteTasks")){
			//归档任务-归档任务按项目查询
			this.myCompleteTasks(service, request, response);
		}else if(method.equals("getTaskSaveInfo")){
			//获得任务保存信息
			this.getTaskSaveInfo(service, request, response);
		}else if(method.equals("getReplyEmps")){
			//获得请假审批人
			this.getReplyEmps(service, request, response);
		}else if(method.equals("addLeaveReply")){
			//添加请假申请
			this.addLeaveReply(service, request, response);
		}else if(method.equals("getMyDataDSWithPersonalTarget")){
			//我的数据 - 个人年度指标
			this.getMyDataDSWithPersonalTarget(service, request, response);
		}else if(method.equals("getMyDateDSWithWorkDistribute")){
			//我的数据 - 工作量分布
			this.getMyDateDSWithWorkDistribute(service, request, response);
		}else if(method.equals("getWaitReceiveTasks")){
			//查询当年待接收状态的任务
			this.getWaitReceiveTasks(service, request, response);
		}else if(method.equals("getWaitReceiveTaskDetails")){
			//查询待接收任务的计划明细
			this.getWaitReceiveTaskDetails(service, request, response);
		}else if(method.equals("saveWaitReceiveTask")){
			//保存接收任务
			this.saveWaitReceiveTask(service, request, response);
		}else if(method.equals("getAuditTasks")){
			//查询当年待审核状态的任务
			//任务保存明细数据格式为actual_date#actual_mh#remark@...
			this.getAuditTasks(service, request, response);
		}else if(method.equals("auditTask")){
			//审核任务 flag = 1通过，0未通过
			this.auditTask(service, request, response);
		}
	}
	
	private void getWaitReceiveTaskDetails(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException {
		String taskId = request.getParameter("taskId");
		List<Map<String,Object>> returnList = service.getWaitReceiveTaskDetails(taskId);
		Gson gson = new Gson();
		String jsonData = gson.toJson(returnList);
		response.getWriter().write(jsonData);
	}

	private void auditTask(IService service, HttpServletRequest request,HttpServletResponse response) throws IOException {
		String taskId = request.getParameter("taskId");
		String reason = request.getParameter("reason");
		String flag = request.getParameter("flag");
		service.auditTask(taskId, reason, flag);
		response.getWriter().write("ok");
	}

	private void getAuditTasks(IService service, HttpServletRequest request,HttpServletResponse response) throws IOException {
		String empId = request.getParameter("empId");
		List<Map<String,Object>> returnList = service.getAuditTasks(empId);
		if(returnList.isEmpty()){
			response.getWriter().write("");
		}else{
			Gson gson = new Gson();
			response.getWriter().write(gson.toJson(returnList));
		}
	}

	private void saveWaitReceiveTask(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException {
		String taskId = request.getParameter("taskId");
		String taskLists = request.getParameter("taskLists");
		service.saveWaitReceiveTask(taskId, taskLists);
		response.getWriter().write("ok");
	}

	private void getWaitReceiveTasks(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException {
		String empId = request.getParameter("empId");
		List<Map<String,Object>> returnList = service.getWaitReceiveTasks(empId);
		if(returnList.isEmpty()){
			response.getWriter().write("");
		}else{
			Gson gson = new Gson();
			response.getWriter().write(gson.toJson(returnList));
		}
	}

	private void loginByForm(IService service, HttpServletRequest request,HttpServletResponse response) throws IOException {
		String u = request.getParameter("u");
		String p = request.getParameter("p");
		Map<String,Object> returnMap = service.loginByForm(u, p);
		Gson gson = new Gson();
		String jsonData = gson.toJson(returnMap);
		System.out.println(jsonData);
		response.getWriter().write(jsonData);
	}

	//登陆
	private void login(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String cellPhone = request.getParameter("cellPhone");
		Map<String,Object> returnMap = service.loginByCellPhone(cellPhone);
		Gson gson = new Gson();
		String jsonData = gson.toJson(returnMap);
		response.getWriter().write(jsonData);
	}
	
	//待办任务
	private void myPendingTask(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String empId = request.getParameter("empId");
		List<Map<String,Object>> returnList = service.myPendingTask(empId);
		if(returnList.isEmpty()){
			response.getWriter().write("");
		}else{
			Gson gson = new Gson();
			response.getWriter().write(gson.toJson(returnList));
		}
	}
	
	//根据任务id查询任务详细信息
	private void getTaskDetailById(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String taskId = request.getParameter("taskId");
		Map<String,Object> returnMap = service.getTaskDetailById(taskId);
		Gson gson = new Gson();
		String jsonData = gson.toJson(returnMap);
		response.getWriter().write(jsonData);
	}
	
	//任务保存
	private void saveTask(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String actual_mh = request.getParameter("actual_mh");
		String actual_date = request.getParameter("actual_date");
		String remark = request.getParameter("remark");
		String proj_task_id = request.getParameter("proj_task_id");
		String proj_id = request.getParameter("proj_id");
		String emp_id = request.getParameter("emp_id");
		Map<String,String> paramMap = new HashMap<String,String>();
		paramMap.put("actual_mh", actual_mh);
		paramMap.put("actual_date", actual_date);
		paramMap.put("remark", remark);
		paramMap.put("proj_task_id", proj_task_id);
		paramMap.put("proj_id", proj_id);
		paramMap.put("emp_id", emp_id);
		service.saveTask(paramMap);
		response.getWriter().write("ok");
	}

	//任务提交
	private void submitTask(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String actual_mh = request.getParameter("actual_mh");
		String actual_date = request.getParameter("actual_date");
		String remark = request.getParameter("remark");
		String proj_task_id = request.getParameter("proj_task_id");
		String proj_id = request.getParameter("proj_id");
		String emp_id = request.getParameter("emp_id");
		String code_size = request.getParameter("code_size");
		Map<String,String> paramMap = new HashMap<String,String>();
		paramMap.put("actual_mh", actual_mh);
		paramMap.put("actual_date", actual_date);
		paramMap.put("remark", remark);
		paramMap.put("proj_task_id", proj_task_id);
		paramMap.put("proj_id", proj_id);
		paramMap.put("emp_id", emp_id);
		paramMap.put("code_size", code_size);
		service.submitTask(paramMap);
		response.getWriter().write("ok");
	}
	
	//归档任务-我参与的项目
	private void myParticipateProjList(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String emp_id = request.getParameter("empId");
		List<Map<String,Object>> returnList = service.myParticipateProjList(emp_id);
		if(returnList.isEmpty()){
			response.getWriter().write("");
		}else{
			Gson gson = new Gson();
			response.getWriter().write(gson.toJson(returnList));
		}
	}
	
	//归档任务-归档任务按项目查询
	private void myCompleteTasks(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String proj_id = request.getParameter("projId");
		String emp_id = request.getParameter("empId");
		List<Map<String,Object>> returnList = service.myCompleteTasks(emp_id,proj_id);
		Gson gson = new Gson();
		String jsonData = gson.toJson(returnList);
		response.getWriter().write(jsonData);
	}
	
	//获得任务保存信息
	private void getTaskSaveInfo(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String taskId = request.getParameter("taskId");
		List<Map<String,Object>> returnList = service.getTaskSaveInfo(taskId);
		Gson gson = new Gson();
		String jsonData = gson.toJson(returnList);
		response.getWriter().write(jsonData);
	}
	
	//获得请假审批人
	private void getReplyEmps(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		String deptId = request.getParameter("deptId");
		List<Map<String,Object>> returnList = service.getReplyEmps(deptId);
		Gson gson = new Gson();
		String jsonData = gson.toJson(returnList);
		response.getWriter().write(jsonData);
	}
	
	//添加请假申请
	private void addLeaveReply(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		Map<String,String> params = new HashMap<String,String>();
		params.put("time_off_type", request.getParameter("time_off_type"));
		params.put("reason", request.getParameter("reason"));
		params.put("ask_emp_id", request.getParameter("ask_emp_id"));
		params.put("reply_emp_id", request.getParameter("reply_emp_id"));
		params.put("main_hour", request.getParameter("main_hour"));
		params.put("free_date", request.getParameter("free_date"));
		service.addLeaveReply(params);
		response.getWriter().write("ok");
	}
	
	//我的数据-个人年度指标
	private void getMyDataDSWithPersonalTarget(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		Map<String,Object> map = service.getMyDataDSWithPersonalTarget(request.getParameter("year"),request.getParameter("empId"));
		Gson gson = new Gson();
		String jsonData = gson.toJson(map);
		response.getWriter().write(jsonData);
	}
	
	//我的数据-工作量分布
	private void getMyDateDSWithWorkDistribute(IService service,HttpServletRequest request, HttpServletResponse response) throws IOException{
		List<Map<String,Object>> list = service.getMyDateDSWithWorkDistribute(request.getParameter("empId"));
		Gson gson = new Gson();
		String jsonData = gson.toJson(list);
		response.getWriter().write(jsonData);
	}
	
	
}
