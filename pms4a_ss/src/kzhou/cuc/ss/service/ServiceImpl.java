package kzhou.cuc.ss.service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import sun.misc.BASE64Encoder;

public class ServiceImpl implements IService {

    private JdbcTemplate jdbcTemplate;
	private TransactionTemplate txTemplate;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
		txTemplate = new TransactionTemplate(new DataSourceTransactionManager(dataSource)); 
   }
    
	
	@Override
	public Map<String, Object> loginByCellPhone(String phoneNum) {
		final StringBuilder sql = new StringBuilder();
		sql.append(" select emp.emp_id,emp.emp_name,emp.dept_id,dept.dept_name from emp_details,emp,dept where emp_details.info_val = ? ");
		sql.append(" and emp_details.emp_id = emp.emp_id ");
		sql.append(" and emp.dept_id = dept.dept_id ");
		sql.append(" and emp.sts = 'A' ");
		try {
			return this.jdbcTemplate.queryForMap(sql.toString(),new Object[]{phoneNum});
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Map<String, Object> loginByForm(String username, String pwd) {
		StringBuilder sql = new StringBuilder();
		sql.append(" select convert(a.emp_id,char) as emp_id,a.emp_name,convert(a.dept_id,char) as depr_id,b.dept_name                                                              ");
		sql.append(" ,convert((select                                                                                                      ");
		sql.append(" concat((select count(*) from proj_task                                                                        ");
		sql.append(" where task_plan_sd between date(date_add(now(),INTERVAL -30 DAY)) and date(date_add(now(),INTERVAL 30 DAY))   ");
		sql.append(" and emp_id = a.emp_id and sts = 'B')                                                                          ");
		sql.append(" ,'条@',                                                                                                       ");
		sql.append(" (select count(*)  from proj_task                                                                              ");
		sql.append(" where task_plan_sd between date(date_add(now(),INTERVAL -30 DAY)) and date(date_add(now(),INTERVAL 30 DAY))   ");
		sql.append(" and emp_id = a.emp_id and sts = 'C')                                                                          ");
		sql.append(" ,'条@',                                                                                                       ");
		sql.append(" (select count(*)  from proj_task                                                                              ");
		sql.append(" where task_plan_sd between date(date_add(now(),INTERVAL -30 DAY)) and date(date_add(now(),INTERVAL 30 DAY))   ");
		sql.append(" and create_emp_id = a.emp_id and sts = 'D'),'条')),char) as task_count                                              ");
		sql.append(" from emp a,dept b where a.dept_id = b.dept_id and a.emp_email = ? and a.emp_pwd = ? and a.sts = 'A'           ");
		try {
			return this.jdbcTemplate.queryForMap(sql.toString(),new Object[]{username,encodeByMd5(pwd)});
		} catch (Exception e) {
			return null;
		}
	}

	private String encodeByMd5(String str) {
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			BASE64Encoder base64en = new BASE64Encoder();
			return base64en.encode(md5.digest(str.getBytes("utf-8")));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> myPendingTask(String empId) {
		final StringBuilder sql = new StringBuilder();
		sql.append("select ");
		sql.append("	convert(proj_task.proj_task_id,char) as proj_task_id ");
		sql.append("	,convert(concat('项目:',proj.proj_name, ' by ' , emp.emp_name),char) as proj_info ");
		sql.append("	,convert(concat('任务:',proj_task.task_item),char) as task_item ");
		sql.append("	,convert(concat('计划工期:',proj_task.task_plan_sd,' 至 ',proj_task.task_plan_fd,'[',proj_task.task_plan_manhour,']'),char) as time_info ");
		sql.append("from proj_task,proj,emp ");
		sql.append("where proj_task.task_plan_sd between date(date_add(now(),INTERVAL -30 DAY)) and date(date_add(now(),INTERVAL 30 DAY)) ");
		sql.append("and proj_task.proj_id = proj.proj_id ");
		sql.append("and proj.proj_state = 1 ");
		sql.append("and proj_task.emp_id = ? ");
		sql.append("and proj_task.sts = 'C' ");
		sql.append("and proj_task.create_emp_id = emp.emp_id ");
		
		
		return this.jdbcTemplate.queryForList(sql.toString(),new Object[]{empId});
	}


	@Override
	public Map<String, Object> getTaskDetailById(String taskId) {
		final StringBuilder sql = new StringBuilder();
		sql.append(" select                                                                                                                                                         ");
		sql.append(" 	 convert(proj_task.proj_task_id,char) as proj_task_id                                                                                                                                    ");
		sql.append(" 	,convert(proj_task.proj_id ,char) as proj_id                                                                                                                                         ");
		sql.append(" 	,convert(proj_task.emp_id,char) as emp_id                                                                                                                                           ");
		sql.append("	,convert(concat('项目:',proj.proj_name, ' by ' , emp.emp_name),char) as proj_info                                                                                                 ");
		sql.append("	,convert(concat('任务名称:',proj_task.task_item),char) as task_item                                                                                                                                        ");
		sql.append(" 	,convert(concat('计划工期:',proj_task.task_plan_sd,' 至 ',proj_task.task_plan_fd),char) as time_info                                                                                  ");
		sql.append(" 	,convert(concat('计划工时:',(convert(proj_task.task_plan_manhour,char)),'/已保存工时:',(convert((ifnull((select sum(task_trace_item.actual_mh) from task_trace_item where proj_task_id = ?),0)),char)),'/剩余工时:',(convert((proj_task.task_plan_manhour - ifnull((select sum(task_trace_item.actual_mh) from task_trace_item where proj_task_id = ?),0)),char))),char) as mh_info  ");
		sql.append(" from proj_task,proj,emp                                                                                                                                        ");
		sql.append(" where proj_task.proj_id = proj.proj_id and proj_task.create_emp_id = emp.emp_id                                                                                ");
		sql.append(" and proj_task.proj_task_id = ?                                                                                                                             ");
		try {
			return this.jdbcTemplate.queryForMap(sql.toString(),new Object[]{taskId,taskId,taskId});
		} catch (Exception e) {
			return new HashMap<String,Object>();
		}
	}


	@Override
	public void saveTask(Map<String, String> paramMap) {
		final String sql = "INSERT INTO `task_trace_item`(`task_trace_item_id`, `actual_mh`, `actual_date`, `crt_time`, `remark`, `proj_task_id`, `proj_id`, `emp_id`)	VALUES (NULL, ?, ?, NOW(), ?, ?, ?, ?)";
		this.jdbcTemplate.update(sql,new Object[]{
				paramMap.get("actual_mh")
				,paramMap.get("actual_date")
				,paramMap.get("remark")
				,paramMap.get("proj_task_id")
				,paramMap.get("proj_id")
				,paramMap.get("emp_id")
		});
	}


	@Override
	public void submitTask(final Map<String, String> paramMap) {
		 txTemplate.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus arg0) {
				try {
					final String sql = "INSERT INTO `task_trace_item`(`task_trace_item_id`, `actual_mh`, `actual_date`, `crt_time`, `remark`, `proj_task_id`, `proj_id`, `emp_id`)	VALUES (NULL, ?, ?, NOW(), ?, ?, ?, ?)";
					jdbcTemplate.update(sql,new Object[]{
							paramMap.get("actual_mh")
							,paramMap.get("actual_date")
							,paramMap.get("remark")
							,paramMap.get("proj_task_id")
							,paramMap.get("proj_id")
							,paramMap.get("emp_id")
					});
					
					//获得任务实际完成工时
					final String sql2 = "select ifnull(sum(actual_mh),0) as actual_mh from task_trace_item where proj_task_id = ?";
					BigDecimal totalActualMH = jdbcTemplate.queryForObject(sql2, new Object[]{paramMap.get("proj_task_id")},BigDecimal.class);
					//更新任务表
					final String sql3 = "update proj_task set task_actual_fd=?,task_actual_manhour=?,task_state='D',sts='D',commit_date=?,submit_type=1,code_size=?,task_end_desc=? where proj_task_id = ?";
					jdbcTemplate.update(sql3,new Object[]{
							paramMap.get("actual_date")
							,totalActualMH
							,paramMap.get("actual_date")
							,paramMap.get("code_size")
							,paramMap.get("remark")
							,paramMap.get("proj_task_id")
					});
				} catch (Exception ex) {
					arg0.setRollbackOnly();
				}				
			}
			 
		 });
		
	}


	@Override
	public List<Map<String, Object>> myParticipateProjList(String empId) {
		final StringBuilder sql = new StringBuilder();
		sql.append(" select distinct convert(proj_task.proj_id,char) as proj_id,proj.proj_name ");
		sql.append(" ,concat('任务个数:',convert(count(proj_task.proj_task_id),char),' / ','计划总工时:',convert(sum(proj_task.task_plan_manhour),char),' / ','实际总工时:',convert(sum(proj_task.task_actual_manhour),char)) as task_info ");
		sql.append(" from proj_task,proj ");
		sql.append(" where proj_task.proj_id = proj.proj_id and proj.proj_state = 1 ");
		sql.append(" and proj_task.emp_id = ? and proj_task.sts = 'E' ");
		sql.append(" group by proj_task.proj_id ");
		
		return this.jdbcTemplate.queryForList(sql.toString(),new Object[]{empId});
	}


	@Override
	public List<Map<String, Object>> myCompleteTasks(String empId, String projId) {
		final StringBuilder sql = new StringBuilder();
		sql.append(" select convert(proj_task.proj_task_id,char) as proj_task_id,concat(proj.proj_name, ' by ' , emp.emp_name) as proj_info,proj_task.task_item ");
		sql.append(" ,convert(concat('计划:',proj_task.task_plan_sd,' 至 ',proj_task.task_plan_fd,'[',proj_task.task_plan_manhour,']',' / 实际:',proj_task.task_actual_sd,' 至 ',proj_task.task_actual_fd,'[',proj_task.task_actual_manhour,']'),char) as time_info ");
		sql.append(" from proj_task,proj,emp ");
		sql.append(" where proj_task.emp_id = ? and proj_task.sts in ('D','E') and proj_task.proj_id = ? ");
		sql.append(" and proj_task.proj_id = proj.proj_id and proj.proj_state = 1 and proj_task.create_emp_id = emp.emp_id ");
		sql.append(" order by proj_task.create_date desc ");
		
		return this.jdbcTemplate.queryForList(sql.toString(),new Object[]{empId,projId});
	}


	@Override
	public List<Map<String, Object>> getTaskSaveInfo(String taskId) {
		List<Map<String,Object>> returnList = new ArrayList<Map<String,Object>>();
		//根据taskId查询所有保存日期和实际保存时期
		final StringBuilder sql = new StringBuilder();
		sql.append(" select distinct d from ( ");
		sql.append(" select DATE_FORMAT(task_date,'%Y-%m-%d') as d from task_detail where proj_task_id = ? ");
		sql.append(" union all ");
		sql.append(" select DATE_FORMAT(actual_date,'%Y-%m-%d') as d from task_trace_item where proj_task_id = ? ");
		sql.append(" ) as t order by d ");
		
		//根据taskId和日期查询计划保存工时
		final StringBuilder sql2 = new StringBuilder();
		sql2.append(" select ifnull(sum(task_manhour),0) from task_detail where proj_task_id = ? and task_date = ? ");
		//根据taskId和日期查询实际保存工时
		final StringBuilder sql3 = new StringBuilder();
		sql3.append(" select ifnull(sum(actual_mh),0)  from task_trace_item where proj_task_id = ? and actual_date = ? ");
		
		List<Map<String,Object>> returnDateList = this.jdbcTemplate.queryForList(sql.toString(),new Object[]{taskId,taskId});
		
		for(Map<String,Object> map : returnDateList){
			Map<String,Object> returnMap = new HashMap<String,Object>();
			returnMap.put("d", map.get("d"));
			returnMap.put("mh", "计划工时：".concat(this.jdbcTemplate.queryForObject(sql2.toString(),new Object[]{taskId,map.get("d")},BigDecimal.class).toString()).concat(" / 实际工时：").concat(this.jdbcTemplate.queryForObject(sql3.toString(),new Object[]{taskId,map.get("d")},BigDecimal.class).toString()));
			returnList.add(returnMap);
		}
		return returnList;
	}


	@Override
	public void addLeaveReply(final Map<String, String> paramMap) {
		//其中free_date使用,分割的字段
		String[] freeDates = paramMap.get("free_date").split(",");
		final List<String> freeDatesList = splitDate(freeDates[0],freeDates[1]);
		//计算总请假工时
		final BigDecimal totalLeaveMH = new BigDecimal(paramMap.get("main_hour")).multiply(new BigDecimal(freeDatesList.size()));
		txTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus arg0) {
				try {
					final String sql = "INSERT INTO vacate_info(time_off_type,reason,sts,crt_date,ask_emp_id,reply_emp_id,main_hour,vacate_scope) VALUES (?,?,'1',NOW(),?,?,?,'1')";
					jdbcTemplate.update(sql,new Object[]{
						paramMap.get("time_off_type")//请假类型
						,paramMap.get("reason")//请假原因
						,paramMap.get("ask_emp_id")//请假人
						,paramMap.get("reply_emp_id")//审核人
						,totalLeaveMH//请假工时
					});
					//获得刚插入的主键
					int pk = jdbcTemplate.queryForInt("select last_insert_id()");
					final String sql2 = "INSERT INTO vacate_detail(free_date,time_off_hour,vacate_info_id,sts) VALUES (?,?,?,'0')";
					for(String freeDate : freeDatesList){
						jdbcTemplate.update(sql2,new Object[]{
								freeDate//请假日期
								,paramMap.get("main_hour")//请假工时
								,pk//外键
							});
					}
				} catch (Exception ex) {
					arg0.setRollbackOnly();
				}				
			}
		});
	}


	@Override
	public List<Map<String, Object>> getReplyEmps(String deptId) {
		//根据deptId获得所在部门的部门经理
		final String sql = "select convert(emp.emp_id,char) as emp_id,emp.emp_name from emp,emp_role where emp.sts = 'A' and emp.dept_id = ? and emp.emp_id = emp_role.emp_id and emp_role.role_id = 1";
		return this.jdbcTemplate.queryForList(sql,new Object[]{deptId});
	}
	
	@SuppressWarnings("static-access")
	private static List<String> splitDate(String psd, String pfd) {
		List<String> returnList = new ArrayList<String>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> list = new ArrayList<Date>();
		Calendar canlandar1 = Calendar.getInstance();
		Calendar canlandar2 = Calendar.getInstance();
		try {
			canlandar1.setTime(castStrToDate(psd));
			canlandar2.setTime(castStrToDate(pfd));

			while (canlandar1.compareTo(canlandar2) < 1) {
				list.add(canlandar1.getTime());
				canlandar1.add(canlandar1.DATE, 1);
			}
			
			for(Date d : list){
				returnList.add(simpleDateFormat.format(d));
			}
			return returnList;
		} catch (Exception e) {
			return null;
		}
		
	}

	private static Date castStrToDate(String str){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return simpleDateFormat.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}

	@Override
	public Map<String, Object> getMyDataDSWithPersonalTarget(String year,String empId) {
		Map<String,Object> returnMap = new HashMap<String,Object>();
		List<String[]> values = new ArrayList<String[]>();
		List<String[]> dates = new ArrayList<String[]>();
		String minM = null;
		String maxM = null;
		//查询指定年度各指标值
		final StringBuilder sql = new StringBuilder();
		sql.append(" select convert(t.id,char) as m	,convert(round(ifnull(I1,0)*100,2),char) as bml	,convert(round(ifnull(L1,0)*100,2),char) as asl,convert(round(ifnull(o1,0)*100,2),char) as aql	,convert(round(ifnull(aa1,0)*100,2),char) as arl from( ");
		sql.append(" select '01' as id union select '02' union select '03' union select '04' union select '05' union select '06' ");
		sql.append(" union select '07' union select '08' union select '09' union select '10' union select '11' union select '12' ");
		sql.append(" ) t left join keguan n on n.emp_id=? and left(n.kh_date,4) = ? and right(n.kh_date,2) = t.id ");		
		List<Map<String,Object>> list = this.jdbcTemplate.queryForList(sql.toString(),new Object[]{empId,year});
		if(list != null && !list.isEmpty()){
			//构造values,dates集合
			int arrayIndex = list.size();
			String[] bml = new String[arrayIndex];
			String[] asl = new String[arrayIndex];
			String[] aql = new String[arrayIndex];
			String[] arl = new String[arrayIndex];
			for(int i = 0;i<arrayIndex;i++){
				bml[i] = list.get(i).get("bml").toString();
				asl[i] = list.get(i).get("asl").toString();
				aql[i] = list.get(i).get("aql").toString();
				arl[i] = list.get(i).get("arl").toString();
			}
			values.add(bml);
			values.add(asl);
			values.add(aql);
			values.add(arl);
			
			//获得月度最小值，最大值
			minM = year + "-" + list.get(0).get("m").toString() + "-01";
			maxM = year + "-" + list.get(list.size()-1).get("m").toString() + "-01";
			
			String[] date4bml = new String[arrayIndex];
			String[] date4asl = new String[arrayIndex];
			String[] date4aql = new String[arrayIndex];
			String[] date4arl = new String[arrayIndex];
			for(int j = 0;j<arrayIndex;j++){
				date4bml[j] = year + "-" + list.get(j).get("m").toString() + "-01";
				date4asl[j] = year + "-" + list.get(j).get("m").toString() + "-01";
				date4aql[j] = year + "-" + list.get(j).get("m").toString() + "-01";
				date4arl[j] = year + "-" + list.get(j).get("m").toString() + "-01";
			}

			dates.add(date4bml);
			dates.add(date4asl);
			dates.add(date4aql);
			dates.add(date4arl);
		}
		
		returnMap.put("values", values);
		returnMap.put("dates", dates);
		returnMap.put("minDate", minM);
		returnMap.put("maxDate", maxM);
		
		return returnMap;
	}


	@Override
	public List<Map<String,Object>> getMyDateDSWithWorkDistribute(String empId) {
		final StringBuilder sql = new StringBuilder();
		sql.append(" select distinct proj_task.proj_id,proj.proj_name                                                  ");
		sql.append(" ,convert(sum(proj_task.task_actual_manhour),char) as mh_info                                    ");
		sql.append("  from proj_task,proj                                                                              ");
		sql.append("  where proj_task.proj_id = proj.proj_id and proj.proj_state = 1                                   ");
		sql.append("  and proj_task.emp_id = ? and proj_task.sts in ('D','E') and proj_task.create_emp_id is not null ");
		sql.append("  group by proj_task.proj_id having(sum(proj_task.task_actual_manhour) >= 80) order by sum(proj_task.task_actual_manhour) desc limit 5                                                                      ");		
		return this.jdbcTemplate.queryForList(sql.toString(),new Object[]{empId});
	}


	@Override
	public List<Map<String, Object>> getWaitReceiveTasks(String empId) {
		final String sql = " select convert(proj_task.proj_task_id,char) as proj_task_id,concat('项目:',proj.proj_name, ' by ' , emp.emp_name) as proj_info,concat('任务:',proj_task.task_item) as task_item  ,convert(concat('计划工期:',proj_task.task_plan_sd,' 至 ',proj_task.task_plan_fd,'[',proj_task.task_plan_manhour,']'),char) as time_info from proj_task,proj,emp where proj_task.proj_id = proj.proj_id and proj.proj_state = 1 and proj_task.create_emp_id = emp.emp_id and proj_task.emp_id = ? and proj_task.sts = 'B' and proj_task.task_plan_sd between date(date_add(now(),INTERVAL -30 DAY)) and date(date_add(now(),INTERVAL 30 DAY)) ";
		return this.jdbcTemplate.queryForList(sql,new Object[]{empId});
	}
	@Override
	public List<Map<String, Object>> getWaitReceiveTaskDetails(String taskId) {
		final String sql = "select convert(task_detail_id,char) as task_detail_id,convert(task_date,char) as task_date,convert(task_manhour,char) as task_manhour,convert(proj_task_id,char) as proj_task_id from task_detail where proj_task_id = ? order by task_detail_id ";
		return this.jdbcTemplate.queryForList(sql,new Object[]{taskId});
	}

	@Override
	public void saveWaitReceiveTask(String taskId,String taskLists) {
		final String sql = "update task_detail set task_manhour = ?,sts_date = now() where task_detail_id = ?";
		final String sql2 = "update proj_task set task_state='C',sts='C',task_actual_sd=now() where proj_task_id = ?";
		
		String[] taskLst = taskLists.split("@");
		for(String taskDetail : taskLst){
			String[] task = taskDetail.split("#");
			this.jdbcTemplate.update(sql,task[1],task[0]);
		}
		this.jdbcTemplate.update(sql2,taskId);
	}


	@Override
	public List<Map<String, Object>> getAuditTasks(String empId) {
		final String sql = "select convert(proj_task.proj_task_id,char) as proj_task_id,concat('项目:',proj.proj_name, ' assign by ' , emp.emp_name) as proj_info,convert(concat('任务:',proj_task.task_item,' action by ',(select emp_name from emp where emp_id = proj_task.emp_id) ),char) as task_info ,convert(concat('计划工期:',proj_task.task_plan_sd,' 至 ',proj_task.task_plan_fd,'[',proj_task.task_plan_manhour,']'),char) as time_info,convert(concat('实际工期:',proj_task.task_actual_sd,' 至 ',proj_task.task_actual_fd,'[实际工时:',proj_task.task_actual_manhour,'/功能点:',code_size,']'),char) as actual_time_info ,(select replace(replace(convert(group_concat(date(actual_date),'#',actual_mh,'#',remark,'@@@'),char),'@@@,','@'),'@@@','') from task_trace_item where proj_task_id = proj_task.proj_task_id) as task_save_info from proj_task,proj,emp where proj_task.proj_id = proj.proj_id and proj.proj_state = 1 and proj_task.create_emp_id = emp.emp_id and proj_task.create_emp_id = ? and proj_task.sts = 'D' and proj_task.task_plan_sd between date(date_add(now(),INTERVAL -30 DAY)) and date(date_add(now(),INTERVAL 30 DAY)) order by proj_task.proj_task_id ";
		return this.jdbcTemplate.queryForList(sql,new Object[]{empId});
	}


	@Override
	public void auditTask(String taskId, String reason, String flag) {
		this.jdbcTemplate.update("{call p_audit_task(?,?,?)}", taskId,reason,flag);		
	}

}
