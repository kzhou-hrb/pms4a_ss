package kzhou.cuc.ss.service;

import java.util.List;
import java.util.Map;

public interface IService {
	/**
	 * 登陆by手机号码
	 * @param phoneNum
	 * @return Map<String,String> - empId,empName,deptName
	 */
	Map<String, Object> loginByCellPhone(String phoneNum);
	
	/**
	 * 通过表单登录
	 * @param phoneNum
	 * @return
	 */
	Map<String, Object> loginByForm(String username,String pwd);
	
	/**
	 * 我的待办任务，查询当前日期前30天后30天的待办任务（sts = C）
	 * @param empId
	 * @return proj_task_id,proj_info,task_item,time_info
	 */
	List<Map<String,Object>> myPendingTask(String empId);
	
	/**
	 * 根据任务id获得任务详细信息
	 * @param taskId
	 * @return
	 */
	Map<String,Object> getTaskDetailById(String taskId);
	
	//根据任务id查询任务拆分信息
	List<Map<String,Object>> getTaskSaveInfo(String taskId);
	
	//保存任务
	void saveTask(Map<String,String> paramMap);
	
	//提交任务
	void submitTask(Map<String,String> paramMap);
	
	//归档任务查询 - 获得指定员工参与的项目列表{projId,projName,taskQty,totalPlanMH,totalActualMH}
	List<Map<String,Object>> myParticipateProjList(String empId);
	
	//归档任务按项目查询
	List<Map<String,Object>> myCompleteTasks(String empId,String projId);
	
	//添加请假申请(仅能向部门经理请假)
	void addLeaveReply(Map<String,String> paramMap);
	//获得请假申请审核人(根据empId找到所在部门的部门经理)
	List<Map<String,Object>> getReplyEmps(String deptId);
	
	//我的数据
	//根据empId获得指定的个人指标数据（"饱满率","按时完成率","按期完成率","按日保存率"）
	//返回的数据结构为Map,其中包括：年月，值，年月最小值，年月最大值，值最小值，值最大值
	Map<String,Object> getMyDataDSWithPersonalTarget(String year,String empId);
	//获得工作量在各个项目中的分布(工作量从大到小前10个项目)
	List<Map<String,Object>> getMyDateDSWithWorkDistribute(String empId);
	
	//查询当前日期前30天后30天的待接收状态的任务(sts = B)
	List<Map<String,Object>> getWaitReceiveTasks(String empId);
	//任务计划明细
	List<Map<String, Object>> getWaitReceiveTaskDetails(String taskId);
	//保存接收任务
	//param:taskLists的数据格式为task_detail_id#task_manhour@...
	void saveWaitReceiveTask(String taskId,String taskLists);
	
	//查询当前日期前30天后30天待审核状态的任务(sts = D)
	//任务保存明细数据格式为actual_date#actual_mh#remark@...
	List<Map<String,Object>> getAuditTasks(String empId);
	//审核任务 flag = 1通过，0未通过
	void auditTask(String taskId,String reason,String flag);

}
